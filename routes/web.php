<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [
	'as' => 'frontend.index',
	'uses' => 'frontend\HomeController@index',
]);
Route::get('restaurant/gridlisting',[
	'as' => 'frontend.restaurant.gridlisting',
	'uses' => 'frontend\RestaurantListingController@gridListing'
]);

Route::get('/adminsection','frontend\AdminSectionController@index')->name('frontend.adminsection');

Route::get('order-step1','frontend\OrderController@order_step1')->name('frontend.order_step1.get');
Route::get('order-step2','frontend\OrderController@order_step2')->name('frontend.order_step2.get');
Route::get('order-step3','frontend\OrderController@order_step3')->name('frontend.order_step3.get');
Route::get('order-datepicker','frontend\OrderController@order_datepicker')->name('frontend.order_datepicker.get');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
