<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantListingController extends Controller
{
    public function gridListing(){
    	return view('frontend.listing.gridlisting');
    }
}
