<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function order_step1()
    {
    	return view('frontend.restaurant.order.order1');
    }

    public function order_step2()
    {
    	return view('frontend.restaurant.order.order2');
    }

    public function order_step3()
    {
    	return view('frontend.restaurant.order.order3');
    }

    public function order_datepicker()
    {
        return view('frontend.restaurant.order.order_datepicker');
    }
}
